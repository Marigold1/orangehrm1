package com.qagroup.orangeHRM;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.qagroup.tools.WebElementUtils;
import io.qameta.allure.Step;

public class AdminPageOrangeHRM {

	// AdminTab
	@FindBy(css = "#menu_admin_viewAdminModule > b")
	private WebElement navigateAdminTab;
	@FindBy(css = "#menu_admin_UserManagement")
	private WebElement navUserManagement;
	@FindBy(css = "#menu_admin_viewSystemUsers")
	private WebElement navUsers;
	@FindBy(css = "#btnAdd")
	private WebElement addUserButton;

	// AddingNewUser
	@FindBy(css = "#systemUser_employeeName_empName")
	private WebElement employeeNameField;
	@FindBy(css = "#systemUser_userName")
	private WebElement userNameField;
	@FindBy(css = "#systemUser_password")
	private WebElement newUserPasswordField;
	@FindBy(css = "#systemUser_confirmPassword")
	private WebElement userConfirmPasswordField;
	@FindBy(css = "#btnSave")
	private WebElement clickSaveButton;

	private WebDriver driver;

	public AdminPageOrangeHRM(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	@Step("Navigate to Admin page")
	public AdminPageOrangeHRM navigatetoAdminTab() {
		navigateAdminTab();
		return new AdminPageOrangeHRM(driver);
	}

	@Step("Navigate to Adminpage")
	public void navigateAdminTab() {
		WebElementUtils.click(navigateAdminTab);
	}

	@Step("Navigate to navUserManagement ")
	public void navUserManagement() {
		WebElementUtils.click(navUserManagement);
	}

	@Step("Add new User")
	public void addUserButton() {
		WebElementUtils.click(addUserButton);
	}

	@Step("Adding new user with  <{employeeName}> and <{newUserName}> and <{newUserPassword}> and <{userConfirmpassword}>")
	public AdminPageOrangeHRM addingNewUserAs(String employeeName, String newUserName, String newUserPassword,
			String userConfirmPassword) {
		
		enterEmployeeNameField(employeeName);
		enterNewUserNameField(newUserName);
		enterNewUserPasswordField(newUserPassword);
		enterUserConfirmPasswordField(userConfirmPassword);
		clickSaveButton();

		return new AdminPageOrangeHRM(driver);
	}

	@Step("Enter Employee Name {employeeName}")
	public void enterEmployeeNameField(String employeeName) {
		WebElementUtils.sendKeys(employeeNameField, employeeName);
	}

	@Step("Enter New User's Name {newUserName}")
	public void enterNewUserNameField(String newUserName) {
		WebElementUtils.sendKeys(userNameField, newUserName);
	}

	@Step("Enter New User Password {newUserPassword}")
	public void enterNewUserPasswordField(String newUserPassword) {
		WebElementUtils.sendKeys(newUserPasswordField, newUserPassword);
	}

	@Step("Enter user Confirm Password {userConfirmPassword}")
	public void enterUserConfirmPasswordField(String userConfirmPassword) {
		WebElementUtils.sendKeys(userConfirmPasswordField, userConfirmPassword);
	}

	@Step("Save new User. Click Button 'Save'")
	public void clickSaveButton() {
		WebElementUtils.click(clickSaveButton);
	}

}
