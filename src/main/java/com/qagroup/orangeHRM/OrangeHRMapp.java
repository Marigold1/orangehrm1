package com.qagroup.orangeHRM;

import org.openqa.selenium.WebDriver;
import com.qagroup.tools.Browser;
import io.qameta.allure.Step;

public class OrangeHRMapp {

	private WebDriver driver;

	public OrangeHRMapp() {
	}

	@Step
	public LoginPageOrangeHRM openLoginPageOrangeHRM() {
		driver = Browser.open();
		driver.get("http://opensource.demo.orangehrmlive.com/");
		return new LoginPageOrangeHRM(driver);
	}

	@Step("Close application/browser")
	public void close() {
		if (driver != null) {
			driver.quit();
			driver = null;
		}
	}

}
