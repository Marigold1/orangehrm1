package com.qagroup.orangeHRM;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.qagroup.tools.WebElementUtils;

import io.qameta.allure.Step;

public class LoginPageOrangeHRM {

	@FindBy(name = "txtUsername")
	private WebElement usernameField;

	@FindBy(xpath = "//*[@id=\"txtPassword\"]")
	private WebElement passwordField;

	@FindBy(css = "#btnLogin")
	private WebElement loginButton;

	@FindBy(css = "#welcome")
	private WebElement welcomeOnHeader;
	@FindBy(css = "#menu_admin_viewAdminModule > b")
	private WebElement navigateAdminTab;

	@FindBy(css = "#welcome-menu > ul > li:nth-child(2) > a")
	private WebElement welcomeOnHeaderLogout;

	private WebDriver driver;

	public LoginPageOrangeHRM(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	@Step("Login as  <{username}> and password <{password}>")
	public LoginPageOrangeHRM loginAs(String username, String password) {

		enterUserName(username);
		enterPassword(password);
		clickLoginButton();
		return new LoginPageOrangeHRM(driver);
	}

	@Step("Enter username {username}")
	public void enterUserName(String username) {
		WebElementUtils.sendKeys(usernameField, username);
	}

	@Step("Enter password {password}")
	public void enterPassword(String password) {
		WebElementUtils.sendKeys(passwordField, password);
	}

	@Step("Click 'Login' button")
	public void clickLoginButton() {
		WebElementUtils.click(loginButton);
	}

	@Step("Greeting on Header")
	public void welcomeOnHeader() {
		WebElementUtils.getText(welcomeOnHeader);
	}

	@Step("Read User Name from header")
	
	public String readwelcomeOnHeader() {
		WebElementUtils.getText(welcomeOnHeader);
		return welcomeOnHeader.getText();
	
	}
	
	@Step("Navigate to Admin page")
	public AdminPageOrangeHRM navigatetoAdminTab() {
		navigateAdminTab();
		return new AdminPageOrangeHRM(driver);
	}
	@Step("Navigate to Adminpage")
	public void navigateAdminTab() {
		WebElementUtils.click(navigateAdminTab);
	}
	
	@Step("User logout")
	public LoginPageOrangeHRM logout() {

		clickWelcomeOnHeader();
		clickWelcomeOnHeaderLogout();

		return new LoginPageOrangeHRM(driver);
	}

	@Step("User logout")
	public void clickWelcomeOnHeader() {
		WebElementUtils.click(welcomeOnHeader);
	}

	public void clickWelcomeOnHeaderLogout() {
		WebElementUtils.click(welcomeOnHeaderLogout);
	}

}
