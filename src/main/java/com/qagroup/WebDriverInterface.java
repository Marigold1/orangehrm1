package com.qagroup;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.github.javafaker.Faker;

public class WebDriverInterface {

	public static void main(String[] args) {
		WebDriver driver = startWebDriver();
		driver.navigate().to("http://www.google.com");

		// for (int i = 0; i < 10; i++)
		// System.out.println(new Faker().company().catchPhrase());
	}

	public static WebDriver startWebDriver() {
		int rand = new Faker().number().numberBetween(0, 2);
		switch (rand) {
		case 0:
			return new ChromeDriver();
		// case 1:
		// return new OperaDriver();
		default:
			return new FirefoxDriver();
		}
	}

}
