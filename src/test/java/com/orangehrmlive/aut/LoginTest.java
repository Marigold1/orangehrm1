package com.orangehrmlive.aut;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import com.qagroup.orangeHRM.AdminPageOrangeHRM;
import com.qagroup.orangeHRM.LoginPageOrangeHRM;
import com.qagroup.orangeHRM.OrangeHRMapp;

public class LoginTest {

	private OrangeHRMapp orangeHRMapp = new OrangeHRMapp();
	private LoginPageOrangeHRM loginPageOrangeHRM;
	private AdminPageOrangeHRM adminPageOrangeHRM;

	@Test
	public void testLogin() {

		loginPageOrangeHRM = orangeHRMapp.openLoginPageOrangeHRM();
		loginPageOrangeHRM = loginPageOrangeHRM.loginAs("Admin", "admin");
		loginPageOrangeHRM.welcomeOnHeader();

		adminPageOrangeHRM =  loginPageOrangeHRM.navigatetoAdminTab();
		adminPageOrangeHRM.navigateAdminTab();
		adminPageOrangeHRM.navUserManagement();
		adminPageOrangeHRM.addUserButton();
		adminPageOrangeHRM = adminPageOrangeHRM.addingNewUserAs("Fiona Grace", "Fiona7Grace3", "Fiona7Grace3","Fiona7Grace3");

		loginPageOrangeHRM = loginPageOrangeHRM.logout();
		loginPageOrangeHRM = loginPageOrangeHRM.loginAs("Fiona7Grace3", "Fiona7Grace3");
		loginPageOrangeHRM.welcomeOnHeader();

		waitFor(1);

		String actualwelcomeOnHeader = loginPageOrangeHRM.readwelcomeOnHeader();
		Assert.assertEquals(actualwelcomeOnHeader, "Welcome Fiona", "Incorrect username");

	}

	@AfterMethod(alwaysRun = true)
	public void tearDown() {
		orangeHRMapp.close();
	}

	private void waitFor(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}